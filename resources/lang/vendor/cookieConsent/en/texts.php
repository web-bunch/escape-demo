<?php

return [
    'message' => 'By using the website you agree to our use of cookies to improve our Services. <a href="/privacy" target="_blank">View our privacy policy</a>',
    'agree' => 'I understand',
];
