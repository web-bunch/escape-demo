// require('./bootstrap');

document.addEventListener('click', function (event) {

  // If the clicked element doesn't have the right selector, bail
  if (!event.target.matches('.day:not(.empty) .js-day-link')) return;

  // Don't follow the link
  event.preventDefault();

  // Log the clicked element in the console
  var day = event.target.getAttribute('data-day');

  var open = document.querySelector('.details.open:not([data-day="' + day + '"])');
  if (open) {
    open.classList.remove("open");
  }
  var active = document.querySelector('.js-day-link.active:not([data-day="' + day + '"])')
  if (active) {
    active.classList.remove("active");
  }
  var delay = open ? 200 : 0;
  setTimeout(function(){
    document.querySelector('.details[data-day="' + day + '"]').classList.toggle("open");
    event.target.classList.toggle("active");
  }, delay);
}, false);

var subbtn = document.getElementById('reservation-form-submit')

var inputs = Array.from(document.getElementsByClassName('form-input'))

var radiosTitle = document.getElementById('team-size-label')
var radios = Array.from(document.getElementsByClassName('form-radio-input'))
var radiosSpans = Array.from(document.getElementsByClassName('radio-span'))
var checked = false

inputs.forEach(function(input) {
  var inputName = input.name.charAt(0).toUpperCase() + input.name.slice(1)
  input.onkeyup = function (e) {
    if (input.value != 0 ) {
      input.labels[0].style.color = ""
      input.labels[0].innerText = input.labels[0].dataset.label
    } else {
      input.labels[0].style.color = "#c7070a"
      input.labels[0].innerText = inputName  + " is required"
    }
  }
})
if (subbtn !== null) {
  subbtn.addEventListener('click', function (event) {
    inputs.forEach(function(input) {
    var inputName = input.name.charAt(0).toUpperCase() + input.name.slice(1)
      if (input.value == 0) {
        input.labels[0].innerHTML = inputName + " is required"
        input.labels[0].style.color = "#c7070a"
      }
    })
    radios.forEach(function(radio) {
    if (radio.checked) {
      checked = true
    }
    radio.onclick = function(e) {
      radiosTitle.style.color = ""
      radiosTitle.innerHTML = "Team size"
    }
  })
    if (!checked) {
      radiosSpans.forEach(function(span) {
        span.style.stroke = "1px red"
      })
      radiosTitle.innerHTML = "Select Team size"
      radiosTitle.style.color = "#c7070a"
    }
  })
}

var viewRooms = document.getElementById('view-rooms')

if (viewRooms !== null) {
  viewRooms.addEventListener('click', event => {
    window.location.href = "#rooms"
  })
}

var checkPromo = document.getElementById('check-promo')

if (checkPromo !== null) {
  checkPromo.addEventListener('click', event => {

    var code = document.getElementById('code').value;

    if (!code) {
      code = "-"
    }

    var data = code
    var params = "code=" + data
    var csrf = document.querySelector('meta[name="csrf-token"]').content;
    var ajaxurl = '/checkpromo/' + code;

    fetch(ajaxurl, {method: 'POST', headers: {'X-CSRF-TOKEN': csrf}})
      .then(response => response.json())
      .then(discount => {
        var prices = Array.from(document.getElementsByClassName('hidden-price'))
        prices.forEach(price => {
          var spanId = 'price-span-' + price.value
          var span = document.getElementById(spanId)
          var final_price = (price.value * (100 - discount)/100).toFixed(2)
          span.innerHTML = final_price
          span.setAttribute('discount.price', final_price)
        })

        if (discount) {
          document.getElementById("ajax-message").innerHTML = "Promo code success";
        } else {
          document.getElementById("ajax-message").innerHTML = "Promo code not valid";
        }
      })
  })
}
