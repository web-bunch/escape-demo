@extends('layout')
@section('pageTitle', 'Instructions before you book | eSCAPE | Online Escape Rooms - by The Web Bunch')
@section('content')
  <div class="container">
    <h1 class="booking-title">Instructions before you book</h1>
    <div class="terms-container">
      <div class="needs">
        <h2 class="inst-title">What you'll need</h2>
        <ul>
          <li>Each player must be at his own personal computer or laptop.</li>
          <li>Solid internet connection.</li>
          <li>Have first-person gaming experience with mouse and keyboard.</li>
          <li>A communication application installed like Discord or Skype. (Discord is preferred)</li>
        </ul>
      </div>
      <div class="needs">
        <h2 class="inst-title">Requirements</h2>
        <ul>
          <li>Minecraft for PC (Java Edition).</li>
          <li>Java.</li>
        </ul>
      </div>
      <div class="needs">
        <h2 class="inst-title">Minecraft: Minimum Requirements</h2>
        <ul>
          <li>CPU: Intel Core i3 3210 | AMD A8 7600 APU or equivalent </li>
          <li>RAM: 4 GB RAM</li>
          <li>HDD: 180 MB to 1 GB available space</li>
          <li>GPU: Intel HD Graphics 4000 or AMD Radeon R5 series | NVIDIA GeForce 400 Series or AMD Radeon HD 7000 series</li>
          <li>OS: 64-bit Windows 7 or later</li>
          <li>Screen Resolution: 1024 x 768 or better</li>
          <li>Network: Broadband Internet connection</li>
        </ul>
      </div>
    </div>
  </div>
@endsection
