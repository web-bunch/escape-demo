@extends('layout')

@section('content')
  @foreach($rooms as $room)
    <div class="container">
       <form class="edit-room" action="/editroom/save" method="POST">
          @csrf
          @method('PUT')
          <div>
            <h2>Room: {{$room->id}}</h2>
          </div>
          <input type="hidden" value="{{$room->id}}" name="id">
          <div>
            Title: <input type="text" value="{{ $room->title }}" name="title">
          </div>
          <div>
            Description: <textarea name="description">{{ $room->description }}</textarea>
          </div>
          <div>
            Duration: <input type="number" value="{{$room->duration}}" name="duration"> minutes
          </div>
          <div>
          Difficulty: <input type="number" value="{{$room->difficulty}}" max="5" min="1" pattern = "\d{1,5}" name="difficulty">
          </div>
          <div>
          Prices:
          </div>
          @foreach($room->prices as $key => $price)
            <div class="edit-room-price">
            {{$key}} Persons <input type="number" name="price-{{$key}}" min="0" value="{{$price}}"> €
            </div>
          @endforeach
          <div>
            Slug: <input type="text" value="{{$room->slug}}" name="slug">
          </div>
          <div>
            Weight: <input type="number" value="{{$room->weight}}" name="weight">
          </div>
          <div>
            <label>
              Published:
              <input type="checkbox" name="published" value='1' {{$room->published ? 'checked' : ''}}>
            </label>
          </div>
          <button class="update-room">Update</button>
          <span class="last-update">Last update: {{$room->updated_at}}</span>
        </form>
    </div>
  @endforeach

@endsection
