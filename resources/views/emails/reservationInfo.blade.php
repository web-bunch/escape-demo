@extends('emails.emailLayout')

@section('content')
<tr style="border-collapse:collapse">
 <td align="left" style="padding:0;Margin:0"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;font-size:30px;font-style:normal;font-weight:normal;color:#404040">Reservation Request</h1></td>
</tr>
<tr style="border-collapse:collapse">
 <td align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:20px;font-size:0">
 <table width="5%" height="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
   <tr style="border-collapse:collapse">
    <td style="padding:0;Margin:0;border-bottom:2px solid #999999;background:#FFFFFFnone repeat scroll 0% 0%;height:1px;width:100%;margin:0px"></td>
   </tr>
 </table></td>
</tr>
<tr style="border-collapse:collapse">
 <td align="left" style="padding:0;Margin:0;padding-bottom:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;line-height:24px;color:#404040">Hi, {{ $data->name }},</p></td>
</tr>

<tr style="border-collapse:collapse">
 <td align="left" style="padding:0;Margin:0;padding-bottom:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;line-height:21px;color:#404040">Thank you for your reservation. One more step and you are good to go.</p></td>
</tr>
<tr style="border-collapse:collapse">
 <td align="left" style="padding:0;Margin:0;padding-bottom:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;line-height:21px;color:#404040">In order to complete your reservation please make a deposit of {{ $data->final_price }}€ via PayPal at escape@web-bunch.com.</p></td>
</tr>
<tr style="border-collapse:collapse">
 <td align="left" style="padding:0;Margin:0;padding-bottom:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;line-height:21px;color:#404040">In the payment description field YOU MUST include your name and your Reservation id ({{ $data->id }}), for us to be able to correctly process the payment. If you want to help us avoid some fees, you can use the friends and family option in PayPal.</p></td>
</tr>
<tr style="border-collapse:collapse">
 <td align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:20px;font-size:0">
 <table width="5%" height="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
   <tr style="border-collapse:collapse">
    <td style="padding:0;Margin:0;border-bottom:2px solid #999999;background:#FFFFFFnone repeat scroll 0% 0%;height:1px;width:100%;margin:0px"></td>
   </tr>
 </table></td>
</tr>
<!-- Text big -->
<tr style="border-collapse:collapse">
 <td align="left" style="padding:0;Margin:0;padding-bottom:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;line-height:24px;color:#404040">Your reservation details are:</p></td>
</tr>
<tr style="border-collapse:collapse">
 <td align="left" style="padding:0;Margin:0;padding-bottom:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;line-height:21px;color:#404040">Reservation id : {{ $data->id }}</p></td>
</tr>
<tr style="border-collapse:collapse">
 <td align="left" style="padding:0;Margin:0;padding-bottom:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;line-height:21px;color:#404040">Room title : {{ $data->room->title }}</p></td>
</tr>
<tr style="border-collapse:collapse">
 <td align="left" style="padding:0;Margin:0;padding-bottom:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;line-height:21px;color:#404040">Date & Time : {{ $data->time }} (GMT+2)</p></td>
</tr>
<tr style="border-collapse:collapse">
 <td align="left" style="padding:0;Margin:0;padding-bottom:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;line-height:21px;color:#404040">Name : {{ $data->name }}</p></td>
</tr>
<tr style="border-collapse:collapse">
 <td align="left" style="padding:0;Margin:0;padding-bottom:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;line-height:21px;color:#404040">Email : {{ $data->email }}</p></td>
</tr>
<tr style="border-collapse:collapse">
 <td align="left" style="padding:0;Margin:0;padding-bottom:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;line-height:21px;color:#404040">Phone : {{ $data->phone }}</p></td>
</tr>
<tr style="border-collapse:collapse">
 <td align="left" style="padding:0;Margin:0;padding-bottom:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;line-height:21px;color:#404040">Team size : {{ $data->team_size }}</p></td>
</tr>
<tr style="border-collapse:collapse">
 <td align="left" style="padding:0;Margin:0;padding-bottom:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;line-height:21px;color:#404040">Final Price : {{ $data->final_price }}€</p></td>
</tr>
@endsection
