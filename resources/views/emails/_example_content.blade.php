<!-- Title -->
<tr style="border-collapse:collapse">
 <td align="left" style="padding:0;Margin:0"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;font-size:30px;font-style:normal;font-weight:normal;color:#404040">Reservation Thing</h1></td>
</tr>

<!-- Small line separator -->
<tr style="border-collapse:collapse">
 <td align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:20px;font-size:0">
 <table width="5%" height="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
   <tr style="border-collapse:collapse">
    <td style="padding:0;Margin:0;border-bottom:2px solid #999999;background:#FFFFFFnone repeat scroll 0% 0%;height:1px;width:100%;margin:0px"></td>
   </tr>
 </table></td>
</tr>

<!-- Text big -->
<tr style="border-collapse:collapse">
 <td align="left" style="padding:0;Margin:0;padding-bottom:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;line-height:24px;color:#404040">Hi, Emy Anderson,</p></td>
</tr>

<!-- Text normal -->
<tr style="border-collapse:collapse">
 <td align="left" style="padding:0;Margin:0;padding-bottom:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;line-height:21px;color:#404040">We've added your phone number to your account, as you asked. Your phone number will be used if you forgot your password and for important account message. Your phone number must be verified.</p></td>
</tr>

<!-- Button -->
<tr style="border-collapse:collapse">
 <td align="left" style="padding:0;Margin:0;padding-top:20px;padding-bottom:20px"><span class="es-button-border" style="border-style:solid;border-color:#4A7EB0;background:#2CB543;border-width:0px;display:inline-block;border-radius:0px;width:auto"><a href="https://escape.web-bunch.com" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:18px;color:#404040;border-style:solid;border-color:#EFEFEF;border-width:10px 25px;display:inline-block;background:#EFEFEF;border-radius:0px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center">Verify phone</a></span></td>
</tr>

<!-- Link (in text) -->
<a href="mailto:escape@web-bunch.com" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;font-size:14px;text-decoration:underline;color:#404040">escape@web-bunch.com</a>
