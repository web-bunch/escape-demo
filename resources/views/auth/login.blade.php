@extends('layout')

@section('content')
<div class="container">
        <h1 class="booking-title">Login</h1>
        <form class="booking-form" method="POST" action="{{ route('login') }}">
            @csrf
            <div class="floating-label">
                <input id="email" type="email" class="form-input @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                <label for="email" class="">E-Mail Address</label>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
            </div>
            <div class="floating-label">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                <label for="password" class="">Password</label>
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
            </div>
            <div class="submit-button-container">
                <button type="submit" class="cta-button">Login</button>
            </div>
        </form>
    </div>
</div>
@endsection
