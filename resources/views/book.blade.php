@extends('layout')
@section('pageTitle', $room->title . ' | Online Escape Room by The Web Bunch')
@section('description', $room->description)
@section('canonical', 'https://escape.web-bunch.com/book/' . $room->slug)
@section('ogimagecontent', url('/' . trim('images/og-' . $room->img, '/\\')))
@section('nobutton', 'nobutton')
@section('content')
@if ($message)
  <div class="message">{{ $message }}</div>
@endif
<div class="container">
  <x-room-full :room="$room" />
</div>
<div class="calendar-container">
  @if ($room->available)
  <x-calendar :reservations="$reservations" />
  @else
  <span class="fully-booked">This room is fully booked right now, please check back soon!</span>
  @endif
</div>
@endsection
