@extends('layout')
@section('pageTitle', 'Terms and Conditions | eSCAPE | Online Escape Rooms - by The Web Bunch')
@section('canonical', 'https://escape.web-bunch.com/terms')
@section('content')
  <div class="container">
    <div class="terms-container">
      <h1 class="booking-title">Terms and Conditions</h1>
      <p>By accessing and using the escape.web-bunch.com domain, our gaming servers, or any other Service provided by eSCAPE, you accept and agree to be bound by the terms and provision of this agreement. In addition, when using our Services, you shall be subject to any posted guidelines or rules applicable to that particular Service. Any participation in our Services will constitute acceptance of this agreement. If you do not agree to abide by the above, please do not use our Services.</p>


      <p>We reserve the right to update these Terms at any time, and you acknowledge and agree that it is your responsibility to review this Site and this Policy periodically and to be aware of any modifications.</p>

      <p>Our Services are intended for users who are at least 18 years old. Persons under the age of 18 are not permitted to use or register for the Services.</p>

      <h2>Definitions</h2>
      <p>"eSCAPE" - The term "eSCAPE" refers to eSCAPE as a whole, which is represented by the eSCAPE Administrative Team. The terms "staff", "game masters", "GMs", “we”, “us”, and “our” refer to eSCAPE and the eSCAPE Administration Team. The terms "member", “user,” “you,” and “your” refer to site visitors, and any other users of our Services.</p>

      <p>"eSCAPE Minecraft Servers" - The term "eSCAPE Minecraft Servers" refers to the Minecraft servers we offer. eSCAPE is not in any way affiliated with Mojang AB, Microsoft, or any other company, copyright, or trademark.</p>

      <p>"Service" and "Services" - The terms "Service" and "Services" refer to any service that eSCAPE offers, which includes our eSCAPE Minecraft Servers, Website, etc.</p>

      <p>"Reservation" - The term "Reservation" refers to a limited time service, booked through escape.web-bunch.com that includes access to one or more of our eSCAPE Minecraft Servers for the time indicated in the reservation request form.</p>

      <h2>Payment Rules</h2>
      <h3>Access to eSCAPE and our Services</h3>
      <p>Do not make a payment of any kind if your Reservation has not been approved and you receive a payment request via email. If you make a payment before a Reservation is approved, that payment may be treated at the discretion of the Administrative Team as a donation, without giving you access to any of our Services.</p>

      <p>In consideration of you paying our fees in accordance with these Terms, you may obtain additional privileges during the Reservation, including access to one or more of our eSCAPE Minecraft Servers. You are not allowed to use any account you are given access to or any other privilege after your Reservation ends. Your Reservation will start on the indicated date and time (subject to earlier termination in accordance with these Terms or to our discretion) will continue until the Reservation expires.</p>

      <p>We may refuse to accept any order by you to make a Reservation at any time at our discretion.</p>

      <p>We may suspend or cancel your Reservation at any time at our discretion for any reason. Except where we suspend or cancel your Reservation as a consequence of your breach of these Terms (in which case no refund will be given), if you have paid a Reservation Fee for a Reservation that relates to a future period of time, we will refund to you the Reservation Fee.</p>

      <p>The fee for your Reservation shall be as set out on our website located at https://escape.web-bunch.com/. The price is set according to group size depending on which experience you are booking for.</p>

      <p>Full payment is required at least 12 hours before the time of your Reservation.</p>

      <h2>Legitimacy of Payments</h2>
      <p>Any payments must be made with the expressed authorization of the account holder or card holder. Payments not made with expressed authorization are not welcomed by eSCAPE and if issues arise eSCAPE reserves the right to remove privileges and restrict or remove the user’s access to eSCAPE services.</p>

      <h2>Payment Values</h2>
      <p>All stated payment values are in Euro (EUR). While eSCAPE does not charge international fees and absorbs any international fees from payments originating outside of the European Union, please be aware that some banks, depending on the method of payment, may still directly charge you a small fee, usually varying with the amount of the payment, which we can neither control nor reimburse.</p>

      <h2>Payment Methods and Process</h2>
      <p>Payments are only accepted via PayPal, which allows quick processing from verified accounts.</p>

      <h2>Processing Time</h2>
      <p>You will receive a payment confirmation mail as soon as your payment is received and processed. If you do not receive confirmation 16 hours after your payment, you should contact us by mail at escape@web-bunch.com.</p>

      <p>Please be aware that PayPal may hold payments from unverified accounts or payments by check until that payment clears with your bank, which can increase processing time by up to two weeks. Unfortunately we have no control over these holds and will not confirm a Reservation until payment is confirmed.</p>

      <h2>Refundability and Cancellation</h2>
      <p>Payments made to eSCAPE are non-refundable unless otherwise allowed in these Terms. eSCAPE will always endeavor to provide a satisfactory outcome as is judged best by the Administrative Team for all players and eSCAPE as a whole. We attempt to be as transparent as possible in our processes and in what you should expect. Even if you are still not satisfied with an outcome, a refund should not be expected, nor will it be given, but please feel free to contact us about the matter.</p>

      <h2>Authority and applicable terms</h2>
      <p>To be eligible to make a Reservation, you must (1) have full legal capacity to enter into a contract in your country of residence; and (2) if you are an individual, be at least 18 years old; and you further represent and warrant to us that you have authority to bind any business on whose behalf you use our site or purchase a Plan to access the Services. Where you do not meet these requirements above you will need to ask a person who does satisfy those requirements to purchase the Plan on your behalf and enter into a contract with us.</p>

      <p>These Terms and any document expressly referred to in them constitute the entire agreement between you and us and supersede and extinguish all previous agreements, promises, assurances, warranties, representations and understandings between us, whether written or oral, relating to its subject matter. You acknowledge that in entering in purchasing a Plan you do not rely on any statement, representation, assurance or warranty (whether made innocently or negligently) that is not set out in these Terms or any document expressly referred to in them. You and we agree that neither of us shall have any claim for innocent or negligent misrepresentation or negligent misstatement based on any statement in these Terms.</p>

      <h2>Donation</h2>
      <p>You also have the option to donate without receiving any service. Any payment received without instruction from the eSCAPE Administrative Team shall be considered a donation without reward(s).</p>

      <h2>Minecraft EULA</h2>
      <p>We have done our best to remain compliant with the Minecraft End User License Agreement (EULA) and will make every effort to remain so. This may include making changes to privileges, with or without notice to you, and any loss of privileges will not be refunded.<p>

      <h2>Our Provision of the Services</h2>
      <p>Our Services are provided on an as-is and as-available basis. We will make reasonable efforts to keep the Services operational at all times. Technical difficulties may result in temporary interruptions to the Services. No interruptions to the Services shall entitle you to a refund of any payment already made by you. We are under no obligation to provide any content for the Services and reserve the right to upload, remove, vary or otherwise deal with any content provided on our Services from time to time. We also reserve the right to modify or discontinue all or part of our Services without notice at any time. We will not be liable to you or any third party for any modification, price change, suspension, or discontinuation of the Services.</p>

      <h2>How We Use Your Personal Information</h2>
      <p>We only use your personal information in accordance with our Privacy Policy. Please take the time to read our Privacy Policy, as it includes important terms which apply to you. By using the Services, you agree to be bound by our Privacy Policy, which is incorporated into these Terms.</p>

      <p>Our Services are hosted in Germany. If you access the Services from any other region of the world with laws or other requirements governing personal data collection, use, or disclosure that differ from applicable laws in Germany, then through your continued use of the Services, you are transferring your data to Germany, and you expressly consent to have your data transferred to and processed in Germany.</p>

      <h2>Confidential Information</h2>
      <p>Any non-personal information or material transmitted to us or through our Services will be deemed to be non-confidential and non-proprietary. By sending us any non-personal information or material, you affirm that you are the legal owner, creator, and/or controller of that information or material and that you give us an unrestricted, irrevocable license to use, reproduce, display, perform, modify, transmit and distribute those materials or information, and you also agree that we are free to use any ideas, concepts, know-how or techniques that you send us for any purpose. However, we will not release your name or otherwise publicize the fact that you submitted materials or other information to us unless: (1) you give us permission to do so, (2) we first notify you that the materials or other information you submit to a particular part of our Services will be published or otherwise used with your name on it, or (3) we are required to do so by law.</p>

      <h2>Other Important Terms</h2>
      <p>To join our eSCAPE Minecraft Servers you will use the Minecraft game, and be also subjected to its EULA.</p>

      <p>We may transfer our rights and obligations under our Contract with you to another organisation, but this will not affect your rights or our obligations under these Terms.</p>

      <p>You may only transfer your rights or your obligations under our Contract with you to another person if we agree in writing.</p>

      <p>This Contract is between you and us. No other person shall have any rights to enforce any of its terms.</p>

      <p>Each of the clauses of these Terms operates separately. If any court or relevant authority decides that any of them are unlawful or unenforceable, the remaining clauses will remain in full force and effect.</p>

      <p>If we fail to insist that you perform any of your obligations under these Terms, or if we do not enforce our rights against you, or if we delay in doing so, that will not mean that we have waived our rights against you and will not mean that you do not have to comply with those obligations. If we do waive a default by you, we will only do so in writing, and that will not mean that we will automatically waive any later default by you.</p>

      <h2>Term and Termination</h2>
      <p>These Terms shall remain in full force and effect while you use our Services. Without limiting any other provision of these Terms, we reserve the right to, at our discretion and without notice or liability, restrict or deny access to and use of our services (including blocking IP addresses), to anyone for any reason, including, without limitation for breach of any part of these Terms or of any applicable law or regulation.</p>

      <p>We may also, at our discretion, suspend, terminate, or delete your account and any created content or information at any time, without warning. If we suspend or terminate your account for any reason, you are prohibited from re-registering or creating a new account. In addition, we reserve the right to take appropriate legal action, including without limitation pursuing civil, criminal, and injunctive redress.</p>

      <h2>Copyright Infringements</h2>
      <p>eSCAPE respects intellectual property rights. If you believe that any content available on or through our Services infringes upon any copyright you own or control, please notify us using by mailing us at escape@web-bunch.com.</p>

      <h2>Liability</h2>
      <p>Nothing in these Terms limits or excludes our liability for: death or personal injury caused by our negligence; or fraud or fraudulent misrepresentation; or any other loss or liability which may not be excluded or limited by law. We will under no circumstances whatsoever be liable to you, whether in contract, tort (including negligence), breach of statutory duty, or otherwise howsoever arising for: any loss of profits, sales, business, or revenue; loss or corruption of data, information or software; loss of anticipated savings; loss of goodwill; or any indirect or consequential loss.</p>

      <p>Our total liability to you, whether in contract, tort (including negligence), breach of statutory duty, or otherwise howsoever arising, shall in no circumstances exceed 100% of the total Reservation Fees paid by you in the 12-month period prior to the date on which the act or omission giving rise to the liability occurred.</p>

      <p>Except as expressly stated in these Terms, we do not give any representation, warranties or undertakings in relation to the Services. Any representation, condition or warranty which might be implied or incorporated into these Terms by statute, common law or otherwise is excluded to the fullest extent permitted by law. In particular, we will not be responsible for ensuring that the content provided through the Services is suitable for your purposes.</p>

      <h2>Indemnification</h2>
      <p>You agree to fully indemnify, hold harmless and defend (collectively “indemnify” and “indemnification”) eSCAPE and its directors, officers, employees, agents, stockholders and Affiliates (collectively, “Indemnified Parties”) from and against all claims, demands, actions, suits, damages, liabilities, losses (including but not limited to any direct, indirect or consequential losses, loss of profit, loss of reputation and all interest, penalties and legal costs (calculated on a full indemnity basis) and all other reasonable professional costs and expenses), settlements, judgments, costs and expenses (including but not limited to reasonable attorney’s fees and costs), whether or not involving a third party claim, which arise out of or relate to (1) any breach of any representation or warranty of eSCAPE contained in this Agreement, (2) any breach or violation of any covenant or other obligation or duty of eSCAPE under this Agreement or under applicable law, (3) your breach or negligent performance or non-performance of any of these Terms, in each case whether or not caused by the negligence of eSCAPE or any other Indemnified Party and whether or not the relevant Claim has merit.</p>

      <h2>Authorization</h2>
      <p>By continued use of this Site and other eSCAPE Services and by making a payment to eSCAPE you indicate your agreement with these Terms and Conditions and you further agree that:
        <ul>
          <li>You are above the age of 18 or 18 years of age, or you are operating with the guidance and consent of a parent or legal guardian.</li>
          <li>By making a payment you have authorization to use the debit/credit card and/or PayPal account.</li>
          <li>By making a payment to eSCAPE you are not entitled to any tangible goods.</li>
          <li>You will not charge back, dispute, or otherwise reverse any payments.</li>
          <li>You understand and agree that you will not be issued a refund.</li>
          <li>You will not abuse and/or misuse any awarded privilege(s).</li>
          <li>We reserve the right to ban any account without notice for any reason.</li>
          <li>You agree to be bound by these Terms and Conditions as listed above.</li>
          <li>If you do not agree with the Terms listed above then do not use our Services. Failure to comply with any of these Terms and Conditions can lead to a permanent ban from all of our Services without refund.</li>
        </ul>
      </p>
      <p>Last Edited on 2020-10-22</p>
    </div>
  </div>
@endsection
