@extends('layout')
@section('nobutton', 'nobutton')
@section('content')
  <h1 class="booking-title">Reserving <span class="highlight">{{$reservation->room->title}}</span><br />for <span class="highlight">{{ $reservation->formated_time }} (GMT+2)</span></h1>
  <p class="change-link-container">
    <a class="change-link" href="{{route('unlock',$reservation->room->slug)}}">Click here to change</a>
  </p>
  <form class="booking-form" id="reservation-form" action="/{{request()->path()}}" method="POST">
    @csrf
    @method('PUT')
    <label class="styled-radios-label" for="team_size" id="team-size-label">Team Size*</label>
    <div class="styled-radios">
      @foreach ($reservation->room->prices as $size => $price)
        <label class="label">
          <input type="radio" name="team_size" value="{{ $size }}" class="form-radio-input radio-{{ $size }}" required>
          <input type="hidden" value="{{ $price }}" class="hidden-price">
          <span class="text radio-span">{{ $size }} players, <span id="price-span-{{ $price }}">{{ number_format($price, 2) }}</span>€</span>
          <span class="checkmark" role="presentation"></span>
        </label>
      @endforeach
    </div>
    <div class="floating-label">
      <input type="text" name="name" id="name" placeholder="What is your name, adventurer?" class="form-input" required>
      <label data-label="Name*" for="name">Name*</label>
    </div>
    <div class="floating-label">
      <input type="tel" name="phone" id="phone" placeholder="+30 690 000 00 00" class="form-input" required>
      <label data-label="Phone*" for="phone">Phone*</label>
    </div>
    <div class="floating-label">
      <input type="email" name="email" id="email" placeholder="me@example.com" class="form-input" required>
      <label data-label="Email*" for="email">Email*</label>
    </div>
    <div class="floating-label">
      <div class="promo-btn-msg">
      <div id="ajax-message"></div>
      <button id="check-promo" class="small-cta-button">Check code</button>
      </div>
      <input type="text" name="promo_code" id="code" placeholder="Promo code" class="something">
      <label data-label="Promo Code" for="code">Promo Code</label>
    </div>
    <label class="terms-label">
      <input type="checkbox" name="terms" id="terms" required>
      <span class="text"><span>I have read and understood the <a href="/terms" target="_blank">Terms & Conditions</a>, <a href="/privacy" target="_blank">Privacy Policy</a> and <a href="/instructions" target="_blank">Instructions before you book</a> pages.</span></span>
      <span class="checkmark"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path fill="none" stroke-miterlimit="10" stroke-width="2" d="M21 6L9 18 4 13"></path></svg></span>
    </label>
    <p class="required-message">* Required field.</p>
    <p class="payment-instructions">You will receive a confirmation email, including payment instructions. We currently only accept payment via PayPal. If payment hasn't been received <span class="highlight">12 hours</span> before the time of your reservation, your reservation will be cancelled.
    </p>
    <div class="submit-button-container">
      <button class="cta-button" type="submit" form="reservation-form" id="reservation-form-submit">Submit</button>
    </div>
  </form>


@endsection
