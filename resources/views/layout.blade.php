<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="favicon.ico" type="image/x-icon" />

        <meta property="og:title" content="@yield('pageTitle', 'eSCAPE | Online Escape Rooms - by The Web Bunch')">
        <meta property="og:description" content="@yield('description', 'A unique experience. Can you escape from our Online Escape Rooms?')">
        <meta property="og:image" content="@yield('ogimagecontent')">

        <title>@yield('pageTitle', 'eSCAPE | Online Escape Rooms - by The Web Bunch')</title>
        <!-- Meta -->
        <meta name="description" content="@yield('description', 'A unique experience. Can you escape from our Online Escape Rooms?')">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:200,500&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Hind:200,500&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Anton&display=swap" rel="stylesheet">
        <!-- Styles -->
        <link rel="stylesheet" href="{{mix('css/app.css')}}">

        <link rel="canonical" href="@yield('canonical')">
    </head>
    <body>
        <div class="page-wrapper">
            <header class="page-header">
                <div class="inner">
                    <a href="/" class="logo"><img width="192" height="66" src="{{asset('images/logo.svg')}}" alt="eSCAPE - by the Web Bunch" /></a>
                    <nav class="menu-main">
                        <ul class="menu">
                            @auth
                            <li class="menu-item">
                                <a href="/emails">Emails</a>
                            </li>
                            <li class="menu-item">
                                <a href="/reservations">Reservations</a>
                            </li>
                            <li class="menu-item">
                                <a href="/editroom">Rooms</a>
                            </li>
                            <li class="menu-item">
                                <a class="dropdown-item logoutbtn" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </li>
                            @endauth
                            {{-- <li class="menu-item"><a class="menu-link" href="#rooms">rooms</a></li>
                            <li class="menu-item active"><a class="menu-link" href="#">faq</a></li>
                            <li class="menu-item"><a class="menu-link" href="#">blog</a></li>
                            <li class="menu-item"><a class="menu-link" href="#">about</a></li>
                            <li class="menu-item"><a class="menu-link" href="#">contact</a></li> --}}
                        </ul>
                    </nav>
                    <nav class="menu-help">
                        @hasSection('nobutton')

                        @else
                            <ul class="menu">
                                <li class="menu-item"><a class="book-button" href="/book">Book Now!</a></li>
                                {{-- <li class="menu-item"><a class="menu-link search" href="#">Search</a></li> --}}
                                {{-- <li class="menu-item"><a class="menu-link login" href="#">Sign in</a></li> --}}
                            </ul>
                        @endif
                    </nav>
                </div>
            </header>
            <div class="page-content">
                @yield('content')
            </div>
            <footer class="page-footer">
                <div class="inner">
                    <a href="/" class="footer-item footer-item--logo logo"><img width="192" height="66" src="{{asset('images/logo.svg')}}" alt="eSCAPE - by the Web Bunch" /></a>
                    <p class="footer-item footer-item--copyright"><span class="copyright-element date">Copyright 2020</span> <span class="copyright-element divider">-</span> <a class="copyright-element link" target="_blank" rel="noopener" href="https://web-bunch.com/">The Web Bunch</a></p>
                    <div class="footer-item">
                        <a class="footer-link" href="/terms">Terms & Conditions</a>
                        <a class="footer-link" href="/privacy">Privacy Policy</a>
                        <a class="footer-link" href="/instructions">Instructions before you book</a>
                    </div>
                    <div class="footer-item footer-item--social">
                        <a href="https://www.facebook.com/escapeWebBunch" target="_blank" rel="noopener" class="link facebook">Follow us on Facebook</a>
                    </div>
                </div>
            </footer>
        </div>
        <script src="{{ mix('/js/app.js') }}"></script>
        @include('cookieConsent::index')
    </body>
</html>
