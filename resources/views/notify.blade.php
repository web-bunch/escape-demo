@extends('layout')
@section('canonical', 'https://escape.web-bunch.com/notify')
@section('content')
<div class="terms-container">
  <h1 class="booking-title">Please give us your email to notify you when the room "{{ $room->title }}" will be published</h1>
  <form class="booking-form" action="/{{request()->path()}}" method="POST">
    @csrf
    @method('PUT')
    <div class="floating-label notify-email">
      <input type="email" name="email" id="email" placeholder=" " class="form-input" required>
      <label data-label="Email" for="email">Email</label>
    </div>
    <div class="terms notify-terms">
      <label class="terms-label">
        <input type="checkbox" name="terms" id="terms" required>
        <span class="text"><span>I have read and understood the <a href="/terms" target="_blank">Terms & Conditions</a> and  <a href="/privacy" target="_blank">Privacy Policy</a></span></span>
        <span class="checkmark"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path fill="none" stroke-miterlimit="10" stroke-width="2" d="M21 6L9 18 4 13"></path></svg></span>
      </label>
    </div>
    <div class="submit-button-container">
      <button class="cta-button" type="submit" id="reservation-form-submit">Submit</button>
    </div>
  </form>
</div>
@endsection
