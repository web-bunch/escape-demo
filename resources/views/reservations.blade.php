@extends('layout')

@section('content')
  <div class="container">
    <h1 class="booking-title">Reservations</h1>
    <div class="all-reservations">
      @foreach($reservations->sortBy('time') as $reservation)
        <div class="admin-reservation {{ isset($reservation->email) ? 'booked' : 'available' }}">
          Time: {{ $reservation->time }},
          Room: {{ $reservation->room->title }},
          Name: {{ $reservation->name }},
          Email: {{ $reservation->email }},
          Phone: {{ $reservation->phone }},
          Team size: {{ $reservation->team_size }},
          isPaid: {{ $reservation->paid }},
          id: {{ $reservation->id }}
        <form class="confirm-reservation" action="/reservations/confirm" method="POST">
          @csrf
          @method('PUT')
          <input name="reservation_id" type="hidden" value="{{ $reservation->id }}">
          <button class="{{ isset($reservation->email) && !$reservation->paid ? '' : 'hidden' }}">Confirm Payment</button>
        </form>
        <form class="delete-reservation" action="/reservations/delete" method="POST">
          @csrf
          @method('PUT')
          <input name="reservation_id" type="hidden" value="{{ $reservation->id }}">
          <button class="{{ isset($reservation->email) ? 'hidden' : ' ' }}">Delete</button>
        </form>
        </div>
      @endforeach
    </div>
    <form class="booking-form" action="/reservations/add" method="POST">
    @csrf
    @method('PUT')

    <div class="floating-label">
      <select type="text" name="room" id="room" class="form-input" required>
        @foreach($rooms as $room)
          <option value="{{ $room->id }}">{{ $room->title }}</option>
        @endforeach
      </select>
      <label for="room">Room</label>
    </div>
    <div class="floating-label">
      <input type="date" name="date" step="1">
      <label for="date">Date</label>
    </div>
    <div class="floating-label">
      <input type="time" name="time" step="1">
      <label for="date">Time</label>
    </div>
    <div class="floating-label">
      <select type="text" name="gm" id="gm" class="form-input" required>
          @foreach($gms as $gm)
            <option value="{{ $gm->id }}">{{ $gm->id }}</option>
          @endforeach
      </select>
      <label for="gm">Gm</label>
    </div>
    <div class="submit-button-container">
      <button class="cta-button" type="submit" id="reservation-form-submit">Submit</button>
    </div>
  </form>

  </div>
@endsection
