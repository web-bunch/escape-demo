@extends('layout')
@section('ogimagecontent', url('/' . trim('images/lazarus-og.jpg', '/\\')))
@section('canonical', 'https://escape.web-bunch.com/')
@section('content')
<div class="hero">
    <div class="cover">
        <div class="inner">
            <h1 class="hero-text">Locked In <br />Online!
                <span class="hero-subtext" id="view-rooms">View Our Online Escape Rooms</span>
            </h1>
        </div>
    </div>
</div>
<section id="rooms" class="frontpage-section frontpage-section--featured-rooms">
    <div class="frontpage-section-inner">
        <h2 class="frontpage-section-title">Our Rooms</h2>
        <div class="room-list">
            @foreach($rooms as $room)
            <x-room-teaser :room="$room" />
            @endforeach
        </div>
    </div>
</section>
<section class="frontpage-section frontpage-section--how-it-works">
    <div class="frontpage-section-inner">
        <h2 class="frontpage-section-title">How It Works</h2>
        <div class="usps">
            <div class="usp usp--locked-in">
                <span class="usp-icon" role="presentation"></span>
                <h3 class="usp-title">The complete escape room experience...</h3>
                <p class="usp-body">You and your friends need to work together to solve puzzles and uncover each room's secrets before the time runs out.</p>
            </div>
            <div class="usp usp--online">
                <span class="usp-icon" role="presentation"></span>
                <h3 class="usp-title">...but online!</h3>
                <p class="usp-body">Play from the comfort of your home. Minimal setup is required and the controls are easy to learn, even with little experience.</p>
            </div>
            <div class="usp usp--moderated">
                <span class="usp-icon" role="presentation"></span>
                <h3 class="usp-title">Our Game Masters are there for you</h3>
                <p class="usp-body">An experienced Game Master will watch and hear you while you play, to provide any needed assistance, so you can focus on having fun.</p>
            </div>
         </div>
    </div>
</section>
<section class="frontpage-section frontpage-section--what-you-need">
    <div class="what-you-need-inner">
        <h2 class="what-you-need-title">EACH PLAYER NEEDS</h2>
        <ul class="what-you-need-list">
            <li class="what-you-need-list-item computer">A WINDOWS, LINUX, OR MACOS PC</li>
            <li class="what-you-need-list-item fps">SOME EXPERIENCE WITH FIRST-PERSON PC GAMES</li>
            <li class="what-you-need-list-item team">PUZZLE-CRACKING AND COMMUNICATION SKILLS</li>
            <li class="what-you-need-list-item fun">A DESIRE TO HAVE FUN!</li>
        </ul>
    </div>
</section>
<section class="frontpage-section frontpage-section--testimonials">
    <div class="frontpage-section-inner">
        <h2 class="frontpage-section-title">Testimonials</h2>
        <div class="testimonials">
            <blockquote class="testimonial">
                <span class="testimonial-quote">“Fairly complex but clear puzzles, interesting lore and a wonderful aesthetic approach. I would recommend Lazarus Mansion to anyone looking for a fun escape room experience.”</span>
                <footer class="testimonial-author">
                    <div class="testimonial-image-container" role="presentation"><img width="78" height="78" class="testimonial-image" src="{{ asset('images/lyfein-thumbnail.jpg') }}" alt="Lyfein"></div>
                    <cite class="testimonial-name">Lyfein</cite>
                </footer>
            </blockquote>
            <blockquote class="testimonial">
                <span class="testimonial-quote">“Love escape rooms, love my couch, gotta love the immersive experience of eSCAPE. The rooms are fun, challenging and pump you up, even though you don't move a muscle.”</span>
                <footer class="testimonial-author">
                    <div class="testimonial-image-container" role="presentation"><img width="78" height="78" class="testimonial-image" src="{{ asset('images/koyniados-thumbnail.jpg') }}" alt="Koyniados"></div>
                    <cite class="testimonial-name">Koyniados</cite>
                </footer>
            </blockquote>
        </div>
    </div>
</section>
<section class="frontpage-section frontpage-section--cta">
    <div class="frontpage-section-inner">
        <h2 class="frontpage-section-title">Are you ready <br />to eSCAPE?</h2>
        <a class="cta-button" href="/book">Book Now</a>
    </div>
</section>
@endsection
