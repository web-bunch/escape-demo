<article class="room room--full">
    <div class="room-info-wrapper">
        <div class="info">
            <h1 class="title">{{ $room->title }}</h1>
            <p class="description">
                {!! $room->description !!}
            </p>
        </div>
        <div class="subtitle">
            <div class="subtitle-element difficulty difficulty-{{ $room->difficulty }}">
                <span class="sr-only">Difficulty: {{ $room->difficulty }}/5</span>
                <div class="locks" role="presentation">
                    <span class="lock"></span>
                    <span class="lock"></span>
                    <span class="lock"></span>
                    <span class="lock"></span>
                    <span class="lock"></span>
                </div>
            </div>
            <p class="subtitle-element duration">
                <span class="sr-only">Duration:</span> <span class="text">{{ $room->duration }}' + 30' setup</span>
            </p>
            <p class="subtitle-element location">
                <span class="sr-only">Location:</span> <span class="text">Online</span>
            </p>

            <div class="subtitle-element pricing">
                <x-pricing-table :prices="$room->prices" />
            </div>
        </div>
    </div>
</article>
