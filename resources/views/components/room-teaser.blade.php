<article class="room room--teaser">
    <div class="info">
        <div class="difficulty difficulty-{{ $room->difficulty }}">
            <span class="sr-only">Difficulty: {{ $room->difficulty }}/5</span>
            <div class="locks" role="presentation">
                <span class="lock"></span>
                <span class="lock"></span>
                <span class="lock"></span>
                <span class="lock"></span>
                <span class="lock"></span>
            </div>
        </div>
        <h1 class="title">{{ $room->title }}</h1>
        <div class="subtitle">
            <p class="subtitle-element team-size">
                <span class="sr-only">Team size:</span> <span class="text">{{ $room->team_size }}</span>
            </p>
            <p class="subtitle-element duration">
                <span class="sr-only">Duration:</span> <span class="text">{{ $room->duration }}'</span>
            </p>
            <p class="subtitle-element location">
                <span class="sr-only">Location:</span> <span class="text">Online</span>
            </p>
        </div>
        <p class="description">
            {!! $room->description !!}
            @if (!$room->published)
            <span class="coming-soon">Coming soon!</span>
            @endif
        </p>
        @if ($room->published)
            @if ($room->available)
                <a href="{{ $room->url }}" class="book-now">Book Now!</a>
            @else
                <span class="fully-booked">This room is fully booked right now, please check back soon!</span>
            @endif
        @else
        <a href="/notify/{{ $room->slug }}" class="notify-me">Notify Me!</a>
        @endif
    </div>
    <div class="image-container"><img width="557" height="400" src="{{asset('images/' . $room->img)}}?v=2" alt="{{ $room->title }} - Online Escape Room"></div>
</article>
