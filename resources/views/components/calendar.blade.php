<h2 class="calendar-title">Select a date:</h2>
<p>(all times are in GMT+2)</p>
<div class="calendar">
  <div class="head">
    <div class="day">SUN</div>
    <div class="day">MON</div>
    <div class="day">TUE</div>
    <div class="day">WED</div>
    <div class="day">THU</div>
    <div class="day">FRI</div>
    <div class="day">SAT</div>
  </div>
  <div class="days">
    @foreach ($reservations as $week => $days)
      <div class="week">
        @foreach ($days as $day => $day_data)
          <div class="day {{implode(' ', $day_data['class'])}}">
            <a href="#" data-day="{{ $day }}" class="day-link js-day-link">{{ $day_data['number'] }}</a>
          </div>
        @endforeach
      </div>
      @foreach ($days as $day => $day_data)
        @if ($day_data['reservations'])
          <div class="details" data-day="{{ $day }}">
            <div class="details-wrapper">
              <div class="details-inner">
                <h3 class="details-title">Available reservations for {{ $day_data['full'] }}<br />(all times are in GMT+2)</h3>
                <ol class="reservation-list">
                  @foreach ($day_data['reservations'] as $time => $reservation_data)
                    <li class="reservation">
                      <span class="time-and-count">
                        <span class="time">{{$time}} - {{$reservation_data['end']}}</span>
                        <span class="count">{{$reservation_data['count']}} available</span>
                      </span>
                      <a class="book-button" href="{{$reservation_data['link']}}">Book now</a></li>
                  @endforeach
                </ol>
              </div>
            </div>
          </div>
        @endif
      @endforeach
    @endforeach
  </div>
</div>
