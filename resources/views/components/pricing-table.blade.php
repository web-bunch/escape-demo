<table class="pricing-table">
    <thead class="pricing-table-head">
        <tr class="pricing-table-row"><th class="pricing-table-cell">Team size</th><th class="pricing-table-cell">Price</th></tr>
    </thead>
    <tbody class="pricing-table-body">
        @foreach ($prices as $people => $price)
            <tr class="pricing-table-row"><td class="pricing-table-cell">{{ $people }}</td><td class="pricing-table-cell">{{ $price }}€</td></tr>
        @endforeach
    </tbody>
</table>
