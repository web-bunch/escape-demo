@extends('layout')

@section('content')
  <h1 class="booking-title">Thank you for your reservation</h1>
  <p class="thankyou-message">The details have been sent to your email.</p>
  <a class="thankyou-back-to-front cta-button" href="/">Back to front</a>
@endsection
