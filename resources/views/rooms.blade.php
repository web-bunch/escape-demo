@extends('layout')

@section('pageTitle', 'Book an Online Escape Room | eSCAPE - by The Web Bunch')
@section('description', 'Can\'t wait to get trapped online? Choose and Book an Online Escape Room now!')
@section('ogimagecontent', url('/' . trim('images/lazarus-og.jpg', '/\\')))
@section('canonical', 'https://escape.web-bunch.com/book')

@section('content')
<section id="rooms" class="frontpage-section frontpage-section--featured-rooms">
    <div class="frontpage-section-inner">
        <h1 class="frontpage-section-title">Book an Online Escape Room</h1>
        <div class="room-list">
            @foreach($rooms as $room)
            <x-room-teaser :room="$room" />
            @endforeach
        </div>
    </div>
</section>
@endsection
