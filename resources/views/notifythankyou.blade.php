@extends('layout')

@section('content')
  <h1 class="booking-title">Thank you for your email submition</h1>
  <p class="thankyou-message">We will notify you when "{{ $room->title }}" is published</p>
  <a class="thankyou-back-to-front cta-button" href="/">Back to front</a>
@endsection
