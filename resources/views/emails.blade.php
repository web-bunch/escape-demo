@extends('layout')

@section('content')
  <div class="container">
    <ul class="notified-users">
      @foreach($notifiedusers as $user)
        <li class="notified-user">
          {{ $rooms->find($user->room_id)->title}}
          {{ $user->email }}

        </li>
      @endforeach
    </ul>
  </div>
@endsection
