<?php

use App\Reservation;
use App\Room;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  $rooms = Room::orderBy('weight')->get();
  return view('welcome', compact('rooms'));
});

Route::get('/book', function () {
  $rooms = Room::where('published', 1)->orderBy('weight')->get();
  return view('rooms', compact('rooms'));
});

Route::get('/book/{room}', 'BookRoomController@show');

Route::get('/book/{room}/unlock', 'BookRoomController@unlock')->name('unlock');

Route::get('/book/{room}/{reservation}', 'BookRoomController@reservation');

Route::put('/book/{room}/{reservation_id}', 'BookRoomController@reservation_id');

Route::get('/thankyou', function () { return view('thankyou'); });

Route::get('/terms', function () { return view('terms'); });

Route::get('/privacy', function () { return view('privacypolicy'); });

Route::get('/instructions', function () { return view('instructions'); });

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/emails', 'EmailsController@show')->middleware('can:view_emails');

Route::get('/editroom', 'EditRoomController@show')->middleware('can:view_emails');

Route::put('/editroom/save', 'EditRoomController@save')->middleware('can:view_emails');

Route::get('/reservations', 'ReservationsController@show')->middleware('can:view_emails');

Route::put('/reservations/add', 'ReservationsController@add')->middleware('can:view_emails');

Route::put('/reservations/confirm', 'ReservationsController@confirm')->middleware('can:view_emails');

Route::put('/reservations/delete', 'ReservationsController@delete')->middleware('can:view_emails');

Route::get('/notify/{room}', 'EmailsController@notify');

Route::put('/notify/{room}', 'EmailsController@adduser');

Route::post('/checkpromo/{code}', 'PromoController@index');

Auth::routes(['register'=>false]);
