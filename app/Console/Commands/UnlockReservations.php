<?php

namespace App\Console\Commands;

use App\Reservation;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UnlockReservations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UnlockReservations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $oldReservations = Reservation::where('time', '<', Carbon::now())
            ->whereNull('reservations.email')
            ->get();

        foreach ($oldReservations as $reservation) {
            $reservation->delete();
        }

        $lockedReservations = Reservation::Locked()->get();
        foreach ($lockedReservations as $reservation) {
                $reservation->locked_at = null;
                $reservation->locked_by = null;
                $reservation->save();
        }

    }
}
