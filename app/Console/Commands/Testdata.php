<?php

namespace App\Console\Commands;

use App\Ability;
use App\Gm;
use App\Reservation;
use App\Role;
use App\Room;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class Testdata extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'testdata:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Room::create([
            'title' => 'Lazarus Mansion',
            'description' => 'Count Lazarus, a deadly vampire, has trapped you in his mansion.<br /><br />You have 90 minutes before he comes back, better get out before then!',
            'duration' => 90,
            'difficulty' => 4,
            'published' => 1,
            'prices' => [
                2 => 12,
                3 => 15,
                4 => 16,
            ],
            'slug' => 'lazarus-mansion',
            'img' => 'lazarus-teaser.jpg',
            'weight' => 20,
        ]);
        Room::create([
            'title' => 'A threat arises',
            'description' => 'A recent series of murders shocked the small town of Shadowville. The killer seems to be copying a legendary one, buried in the local cemetery.<br><br>Can you uncover the truth and catch him before more people die?',
            'duration' => 60,
            'difficulty' => 3,
            'published' => 1,
            'prices' => [
                2 => 10,
                3 => 12,
                4 => 14,
            ],
            'slug' => 'a-threat-arises',
            'img' => 'undead-teaser.jpg',
            'weight' => 0,
        ]);
        Room::create([
            'title' => 'The museum',
            'description' => 'Uncover the secrets hidden behind the museum\'s art!',
            'duration' => 60,
            'difficulty' => 3,
            'published' => 0,
            'prices' => [
                2 => 10,
                3 => 12,
                4 => 14,
            ],
            'slug' => 'the-museum',
            'img' => 'museum-teaser.jpg',
            'weight' => 100,
        ]);
        Gm::create();
        $pass = Hash::make('12345678');
        $user = User::firstOrCreate(
            ['email' => 'admin@web-bunch.com'],
            ['name' => 'admin',
            'password' => $pass ]);
        $ability = Ability::firstOrCreate(['name' => 'view_emails']);
        $role = Role::firstOrCreate(['name'=> 'admin']);
        $role->abilities()->sync([$ability->id]);
        $user->roles()->sync([$role->id]);
        Reservation::create(['room_id' => 1, 'gm_id' => 1, 'email' => 'foo@example.com', 'time' => Carbon::tomorrow()]);
        Reservation::create(['room_id' => 1, 'gm_id' => 1, 'time' => Carbon::parse('9pm')->addDays(2)]);
        Reservation::create(['room_id' => 1, 'gm_id' => 1, 'time' => Carbon::parse('9.30pm')->addDays(2)]);
        Reservation::create(['room_id' => 1, 'gm_id' => 1, 'time' => Carbon::parse('8.30pm')->addDays(2)]);
        Reservation::create(['room_id' => 1, 'gm_id' => 1, 'time' => Carbon::parse('7.30pm')->addDays(2)]);
        Reservation::create(['room_id' => 1, 'gm_id' => 1, 'time' => Carbon::parse('6.30pm')->addDays(2)]);
        Reservation::create(['room_id' => 1, 'gm_id' => 1, 'time' => Carbon::parse('5.30pm')->addDays(2)]);
        Reservation::create(['room_id' => 1, 'gm_id' => 1, 'time' => Carbon::parse('4.30pm')->addDays(2)]);
        Reservation::create(['room_id' => 1, 'gm_id' => 1, 'time' => Carbon::parse('3.30pm')->addDays(2)]);
        Reservation::create(['room_id' => 1, 'gm_id' => 1, 'time' => Carbon::parse('2.30pm')->addDays(2)]);
        Reservation::create(['room_id' => 1, 'gm_id' => 1, 'time' => Carbon::parse('1.30pm')->addDays(2)]);
        Reservation::create(['room_id' => 1, 'gm_id' => 1, 'time' => Carbon::parse('12.30pm')->addDays(2)]);
        Reservation::create(['room_id' => 1, 'gm_id' => 1, 'time' => Carbon::parse('11.30pm')->addDays(2)]);
        Reservation::create(['room_id' => 1, 'gm_id' => 1, 'time' => Carbon::parse('10.30pm')->addDays(2)]);
        Reservation::create(['room_id' => 1, 'gm_id' => 1, 'time' => Carbon::parse('9.30pm')->addDays(2)]);
        Reservation::create(['room_id' => 1, 'gm_id' => 1, 'time' => Carbon::parse('9.30pm')->addDays(3)]);
    }
}
