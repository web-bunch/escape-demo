<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model {
  protected $guarded = [];

  public function room() {
    return $this->belongsTo('App\Room');
  }

  public function scopeLocked($query) {
    $query->whereNotNull('reservations.locked_at')->where('locked_at', '<', Carbon::now()->sub('10 minutes'));
  }

  public function scopeReserved($query) {
    $query->whereNotNull('reservations.email');
  }

  public function getReservedAttribute() {
    return isset($this->email);
  }

  public function scopeAvailable($query) {
    $this->addAvailableRestrictionToQuery($query);
  }

  public function isAvailable() {
    return !$this->email
      && $this->time > Carbon::now()->addHours(24)
      && (!$this->locked_at || $this->locked_by == request()->session()->getId());
  }

  public function lock() {
    $this->locked_at = Carbon::now();
    $this->locked_by = request()->session()->getId();
    $this->save();
  }

  public function unlock($save = true) {
    if ($this->locked_by != request()->session()->getId()) {
      abort(403);
    }
    $this->locked_at = null;
    $this->locked_by = null;
    if ($save) {
      $this->save();
    }
    return $this;
  }

  public function getFormatedTimeAttribute() {
  return (new Carbon($this->time))->format('d/m/Y \a\t H:i');
  }

  public static function addAvailableRestrictionToQuery($query) {
    $query
      ->whereNull('reservations.email')
      ->where('reservations.time', '>', Carbon::now()->addHours(24))
      ->whereNull('reservations.locked_at');
  }
}
