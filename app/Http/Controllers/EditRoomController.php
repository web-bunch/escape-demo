<?php

namespace App\Http\Controllers;

use App\Room;
use Illuminate\Http\Request;

class EditRoomController extends Controller
{
    public function show() {
      $rooms = Room::all();

      return view('editroom', compact('rooms'));
    }

    public function save() {
      $id = request('id');
      $room = Room::find($id);

      $prices = [];
      foreach ($room->prices as $key => $price) {
        $prices[$key] = request('price-'.$key);
      }
      $room->prices = $prices;
      $room->title = request('title');
      $room->description = request('description');
      $room->duration = request('duration');
      $room->difficulty = request('difficulty');
      $room->published = request('published') ?? 0;
      $room->slug = request('slug');
      $room->weight = request('weight');
      $room->save();


      return redirect('editroom');
    }
}
