<?php

namespace App\Http\Controllers;

use App\Notifieduser;
use App\Room;
use Illuminate\Http\Request;

class EmailsController extends Controller
{
    public function show() {
      $notifiedusers = Notifieduser::all();
      $rooms = Room::all();
      return view('emails', compact('notifiedusers', 'rooms'));
    }

    public function adduser($room) {
      $room  = Room::where('slug', $room)
        ->firstOrFail();

      $notifieduser = Notifieduser:: firstOrCreate([
        'email' => request('email'),
        'room_id' => $room->id,
      ],[
        'notified' => FALSE
      ]);

      return view('notifythankyou', compact('room'));
    }

    public function notify($room) {
      $room  = Room::where('slug', $room)
        ->firstOrFail();

      if ($room->published == 1) {
        return redirect($room->url);
        } else {
        return view('notify', compact('room'));
        }
    }

}
