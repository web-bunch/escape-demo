<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PromoController extends Controller
{
  public const PROMO_CODES = [
    'PROMO' => 10,
    'TZAMPATZIDES' => 15,
    'EARLY BIRD' => 20,
    'WELCOME BACK' => 30,
    'KOROMPOS' => 50,
    // 'KONTAROS' => 100,
  ];

  public function index($code) {
    return static::PROMO_CODES[$code] ?? 0;
  }
}
