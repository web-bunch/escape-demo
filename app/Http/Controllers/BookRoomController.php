<?php

namespace App\Http\Controllers;

use App\Mail\ClientMail;
use App\Mail\NewBooking;
use App\Room;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BookRoomController extends Controller
{
    public function show($room) {
      $room  = Room::where('slug', $room)->firstOrFail();

      if (!$room->published) {
        return redirect('notify/' . $room->slug);
      }

      // @todo make this cache friendly
      $locked = $room->reservations()
        ->where('locked_by', request()->session()->getId())
        ->first();

      if ($locked) {
        return redirect($room->url . '/' . $locked->id);
      }

      if (!$room->available) {
        return view('book', [
          'room' => $room,
          'reservations' => [],
          'message' => request()->session()->pull('message',false),
        ]);
      }

      $reservations = $room->availableReservations()->orderBy('time', 'asc')->pluck('time','id');

      $grouped = [];
      $day = Carbon::parse('Monday this week')->addDays(-1);
      for ($i=0; $i < 35; $i++) {
        $grouped[$day->week()][$day->toDateString()] = [
          'class' => [],
          'reservations' => [],
          'number' => $day->format('d'),
          'full' => $day->format('d/m/Y'),
        ];
        $day->addDays(1);
      }

      foreach ($reservations as $id => $time) {
        $reservation_time = new Carbon($time);
        $end_time = (new Carbon($time))->addMinutes($room->duration + 30);

        $grouped[$reservation_time->week()][$reservation_time->toDateString()]['reservations'][$reservation_time->toTimeString('minute')]['link'] = $room->url . '/' . $id;
        $grouped[$reservation_time->week()][$reservation_time->toDateString()]['reservations'][$reservation_time->toTimeString('minute')]['end'] = $end_time->toTimeString('minute');
        $grouped[$reservation_time->week()][$reservation_time->toDateString()]['reservations'][$reservation_time->toTimeString('minute')]['count'] =
          ($grouped[$reservation_time->week()][$reservation_time->toDateString()]['reservations'][$reservation_time->toTimeString('minute')]['count'] ?? 0) + 1;
      }

      $now = Carbon::now();
      $grouped[$now->week()][$now->toDateString()]['class'][] = 'today';

      foreach ($grouped as $week => $week_data) {
        foreach ($week_data as $date => $date_data) {
          $count = 0;
          foreach ($date_data['reservations'] as $reservation) {
            $count += $reservation['count'];
          }
          switch ($count) {
            case 0:
              $grouped[$week][$date]['class'][] = 'empty';
              break;

            case 1:
              $grouped[$week][$date]['class'][] = 'last';
              break;

            default:
              $grouped[$week][$date]['class'][] = 'available';
              break;
          }
        }
      }
      return view('book', [
        'room' => $room,
        'reservations' => $grouped,
        'message' => request()->session()->pull('message',false),
      ]);
    }

  public function unlock($room) {
    $room  = Room::where('slug', $room)->where('published', 1)->firstOrFail();

    // @todo make this cache friendly
    $locked = $room->reservations()
      ->where('locked_by', request()->session()->getId())
      ->first();

    if ($locked) {
      $locked->unlock();
    }

    return redirect($room->url);
  }

  public function reservation($room, $reservation) {
    $room  = Room::where('slug', $room)
      ->where('published', 1)
      ->firstOrFail();

    $locked = $room->reservations()
      ->where('locked_by', request()->session()->getId())
      ->first();

    if ($locked && $locked->id != $reservation) {
      $locked->unlock();
    }

    $reservation = $room->reservations()
      ->where('id', $reservation)
      ->firstOrFail();

    if (!$reservation->isAvailable()) {
      request()->session()->flash('message', 'Someone got that reservation! Please select another time.');
      return redirect($room->url);
    }

    $reservation->lock();

    return view('reservation_form', compact('reservation'));
  }

  public function reservation_id($room, $reservation_id) {
    $room  = Room::where('slug', $room)
      ->where('published', 1)
      ->firstOrFail();

    $reservation = $room->reservations()
      ->where('locked_by', request()->session()->getId())
      ->first();

    if (!$reservation || $reservation->id != $reservation_id) {
      request()->session()->flash('message', 'Something went wrong! Please try again.');
      return redirect($room->url);
    }

    if (!$reservation->isAvailable()) {
      request()->session()->flash('message', 'Something went wrong! Please try again.');
      return redirect($room->url);
    }

    $data = request()->validate([
      'phone' => 'required',
      'email' => 'required',
      'name' => 'required',
      'team_size' => 'required',
      'promo_code' => 'nullable',
    ]);

    $discount = \App\Http\Controllers\PromoController::PROMO_CODES[$data['promo_code']] ?? 0;

    $data['final_price'] =  floor($room->prices[$data['team_size']] * (100 - $discount))/100;

    $reservation->unlock(false)->update($data);

    $emails = User::whereHas('roles', function($q){
    $q->where('name', '=', 'admin');
    })->pluck('email');

    foreach ($emails as $email) {
      \Mail::to($email)->send(new ClientMail($reservation, 'emails.newbooking', 'New Booking'));
    }

    \Mail::to($data['email'])->bcc('escape+bcc@web-bunch.com')->send(new ClientMail($reservation, 'emails.reservationInfo', 'eSCAPE Reservation Info'));

    return redirect('thankyou');
  }
}


