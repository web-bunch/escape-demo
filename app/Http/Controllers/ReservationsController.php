<?php

namespace App\Http\Controllers;

use App\Gm;
use App\Mail\ClientMail;
use App\Reservation;
use App\Room;
use Illuminate\Http\Request;

class ReservationsController extends Controller
{
  public function show() {
    $reservations = Reservation::get();
    $rooms = Room::all();
    $gms = Gm::all();

    return view('reservations', compact('reservations', 'rooms','gms'));
  }

  public function confirm() {
    $id = request('reservation_id');
    $reservation = Reservation::find($id);
    $reservation->paid = true;
    $reservation->save();

    \Mail::to($reservation->email)->bcc('escape+bcc@web-bunch.com')->send(new ClientMail($reservation, 'emails.confirmationEmail', 'Reservation Confirmation'));

    return redirect('reservations');
  }

  public function add() {
    Reservation::create([
            'room_id' => request('room'),
            'gm_id' => request('gm'),
            'time' => request('date') . " " . request('time'),
        ]);

    return redirect('reservations');
  }

  public function delete() {
    $id = request('reservation_id');
    Reservation::find($id)->delete();

    return redirect('reservations');
  }
}
