<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model {
  protected $guarded = [];

  protected $casts = [
      'prices' => 'array',
  ];

  public function reservations() {
    return $this->hasMany('App\Reservation');
  }

  public function availableReservations() {
    return $this->reservations()->available();
  }

  public function getAvailableAttribute() {
    return $this->availableReservations()->exists();
  }

  public function getTeamSizeAttribute() {
    $sizes = array_keys($this->prices);
    return min($sizes) . '-' . max($sizes);
  }

  public function getUrlAttribute() {
    return '/book/' . $this->slug;
  }

  public function availableGms() {
    $relation = $this->belongsToMany('App\Gm', 'reservations');
    Reservation::addAvailableRestrictionToQuery($relation);

    return $relation;
  }
}
