<?php

namespace App\View\Components;

use Illuminate\View\Component;

class RoomTeaser extends Component
{
    /**
     * The room.
     *
     * @var App\Room
     */
    public $room;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($room)
    {
        $this->room = $room;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.room-teaser');
    }
}
