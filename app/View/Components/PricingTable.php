<?php

namespace App\View\Components;

use Illuminate\View\Component;

class PricingTable extends Component
{
    /**
     * The prices, keyd by people.
     *
     * @var array
     */
    public $prices;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($prices)
    {
        $this->prices = $prices;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.pricing-table');
    }
}
