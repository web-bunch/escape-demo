<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Calendar extends Component
{
    /**
     * The calendar reservations
     *
     * @var array
     */
    public $reservations;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($reservations)
    {
        $this->reservations = $reservations;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.calendar');
    }
}
