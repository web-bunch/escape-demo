<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gm extends Model {
  public function reservations() {
    return $this->hasMany('App\Reservation');
  }

  public function availableReservations() {
    return $this->reservations()->available();
  }
}
